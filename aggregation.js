//[SECTION] MongoDB Aggregation

/*
  -Used to generate, manipulate, and perform operations to create filtered results that helps us to analyze the data.

  */

// Using the aggregate method:
/*
  - the "$match" is used to pass the document that meet the specified condition(s) to the next stage/aggregation process.

  -Syntax:
  {$match: {field:value}}

  -The "$group"
  -Syntax: {$group: {_id:"value", fieldResult:"valueResult"}}

*/


db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id:"$supplier_id",total:{$sum:"$stock"}}}
]);


//field projection with Aggregation

/*
  -The $project can be used when aggregating data to include/exclude fields from the returned results.

  -Syntax:
  {$project: {field:1/0}}

*/

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id:"$supplier_id",total:{$sum:"$stock"}}},
  {$project: {_id:0}}
]);


// Sorting aggregated results
/*
  -The $sort can be used to change of the aggregated result

  -syntax:
  {$sort: {field:1}}
*/

db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id:"$supplier_id",total:{$sum:"$stock"}}},
  {$sort: {total:-1}}
]);

//Aggregating results based on array fields.

/*
  -the "$unwind" deconstructs an array field from a collection/field with an array value to output a result.
    -Syntax:
    -{$unwind:field}

1*/


db.fruits.aggregate([
  {$unwind: "$origin"}
]);

db.fruits.aggregate([
  {$unwind: "$origin"},
  {$group:{_id:"$origin", kinds:{$sum:1}}}
]);

//[SECTION] Other aggregate stages

//$count all yellow fruits
db.fruits.aggregate ([
  {$match: {color:"Yellow"}},
  {$count: "Yellow Fruits"}
]);


// $avg gets the average value of $stock
db.fruits.aggregate ([
  {$match: {color:"Yellow"}},
  {$group:{_id:"color", yellow_fruits_stock:{$max:"$stock"}}}
])

//$min & $max
